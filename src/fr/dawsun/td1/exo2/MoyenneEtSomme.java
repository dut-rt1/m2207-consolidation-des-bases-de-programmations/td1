package fr.dawsun.td1.exo2;

public class MoyenneEtSomme {
	
	
	public static void main(String[] args) {
		
		int somme = 0;
		double moyenne = 0;
		
		for(int i = 1; i <= 100; i++) {
			somme += i;
		}
		
		moyenne = somme/100;
		
		System.out.println("Somme: " + somme + " - Moyenne: " + moyenne);
		
	}
	
	

}
