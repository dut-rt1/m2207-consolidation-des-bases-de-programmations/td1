package fr.dawsun.td1.exo3;

import java.util.Scanner;

public class SaisieClavier {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Entier>");
		int entier = sc.nextInt();
		System.out.println("R�el>");
		double reel = sc.nextDouble();
		System.out.println("Pr�nom>");
		String prenom = sc.next();
		
		System.out.println("Bonjour " + prenom + " ! La somme de " + entier + " et " + reel + " est " + (entier+reel));
		
	}

}
