package fr.dawsun.td1.exo6;

public class MesNotes {
	
	
	public static int[] tabNotes = {11,12,2,5,18,14,15,8,13,15};
	
	
	public static void main(String[] args) {
		
		System.out.println("Moyenne: " + getMoyenne());
		System.out.println("Note minimale: " + getMinNote());
		System.out.println("Note maximale: " + getMaxNote());
		
	}
	
	
	public static double getMoyenne() {
		
		int somme = 0;
		double moyenne = 0;
		
		for(int i = 0; i < tabNotes.length; i++) {
			somme += tabNotes[i];
		}
		
		moyenne = somme/(tabNotes.length);
		
		return moyenne;
		
	}
	
	public static int getMinNote() {
		
		int min = tabNotes[0];
		
		for(int i = 0; i < tabNotes.length; i++) {
			if(tabNotes[i] < min) {
				min = tabNotes[i];
			}
		}
		
		return min;
		
	}
	
	public static int getMaxNote() {
		
		int max = tabNotes[0];
		
		for(int i = 0; i < tabNotes.length; i++) {
			if(tabNotes[i] > max) {
				max = tabNotes[i];
			}
		}
		
		return max;
		
	}

}
