package fr.dawsun.td1.exo4;

import java.util.Random;
import java.util.Scanner;

public class DevineLeNombre {
	
	public static void main(String[] args) {
		
		boolean run = true;
		
		int essaie = 0;
		
		int r = new Random().nextInt(20)+1;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("##########################################");
		System.out.println("############[GUESS THE NUMBER]############");
		System.out.println("##########################################");
		
		System.out.println("");
		System.out.println("Devinez le nombre auquel je penses, il est compris entre 0 et 20");
		
		
		while(run) {
			
			
			System.out.println("Prposition> ");
			int proposition = sc.nextInt();
			
			if(proposition == r) {
				run = false;
			}else {
				essaie++;
				System.out.println("Non ce n'est pas �a !");
			}
			
			
		}
		
		System.out.println("F�licitations, vous avez trouv� le nombre auquel je pensais, le " + r);
		
	}

}
