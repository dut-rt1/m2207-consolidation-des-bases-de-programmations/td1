package fr.dawsun.td1.exo1;

public class Main {
	
	static String nom = "bob";
	static int age = 18;
	static double poids = 58;
	static double taille = 1.82;
	
	public static void main(String[] args) {
		
		
		
		System.out.println(nom + " a " + age + " ans, fait " + poids + " Kg et il lui reste " + (65-age) + " ans avant la retraite");
		
		int tempAge = age;
		
		//exo4
		for(double i = poids; i < 70; i += 1.2) {
			tempAge++;
			System.out.println("age: " + tempAge + " - poids actuel: " + i);
		}
		
		System.out.println(nom + " d�passera les 70Kg � ses " + tempAge + " ans");
		
		
		//exo5
		for(int  i = age; i < 40; i += 1) {
			age = i;
			poids += 1.2;
		}
		
		
		
		System.out.println("IMC de Bob: " + getIMC());
		
		//Exo6
		if(getIMC() <= 25) {
			System.out.println("Bob a une corpulance normale");
		}else {
			System.out.println("Bob a une corpulance trop forte");
		}
		
		//exo7
		int km = 0;
		while(getIMC() > 24) {
			km++;
			poids-=0.24;
		}
		
		System.out.println("IMC: " + getIMC() + " - Poids:" + poids + " - km:" + km);
		
		
		
		
		
	}
	
	public static double getIMC() {
		return Math.floor(poids/Math.pow(taille, 2));
	}

}
