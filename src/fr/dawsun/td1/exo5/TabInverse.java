package fr.dawsun.td1.exo5;

public class TabInverse {
	
	public static void main(String[] args) {
		
		int[] tab = {1, 2, 3, 4};
		
		String list = "";
		
		for(int i = 0; i < tab.length; i++) {
			list += " " + tab[i];
		}
		
		System.out.println(list);
		
		for(int i = 0; i < tab.length/2; i++) {
			int temp = tab[i];
			tab[i] = tab[(tab.length-1)-i];
			tab[tab.length-1-i] = temp;
		}
		
		list = "";
		
		for(int i = 0; i < tab.length; i++) {
			list += " " + tab[i];
		}
		
		System.out.println(list);
		
		
		
		
	}

}
