package fr.dawsun.td3;

public class TestAnimal {
	
	public static void main(String args[]) {
		
		Animal ani = new Animal();
		
		
		//It works because the animal class extends the class object
		System.out.println(ani.toString());
		
		//testing affiche()
		ani.affiche();
		
		//testing cri()
		System.out.println("Cri animal: " + ani.cri());
		
		Chien chien = new Chien();
		
		//Showing the same result that before, same method, method belongs to the animal class
		chien.affiche();
		//cri() have to be public to acces it
		System.out.println("Cri chien: " + chien.cri());
		
		//testing origin
		System.out.println("Origine animal: " + ani.origin());
		System.out.println("Origine chien: " + chien.origin());
		
		//Chat
		Chat chat = new Chat();
		
		chat.affiche();
		
		//testing cri
		System.out.println("Cri animal: " + ani.cri());
		System.out.println("Cri chien: " + chien.cri());
		System.out.println("Cri chat: " + chat.cri());
		System.out.println("Miaulement chat: " + chat.miauler());
		
		/* Animal can't use miauler(), miauler() belongs to the chat class
		System.out.println("Miaulement animal: " + ani.miauler());
		*/
		
		
		
	}

}
