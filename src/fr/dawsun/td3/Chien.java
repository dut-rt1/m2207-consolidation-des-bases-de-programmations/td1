package fr.dawsun.td3;

public class Chien extends Animal{
	
	//By adding those method, we replace the method with the same name in Animal
	
	public void affiche() {
		System.out.println("Je suis un chien");
	}
	
	public String cri() {
		return "Ouaf !, Ouaf !";
	}

}
