package fr.dawsun.td3;

public class Animal {
	
	
	public void affiche() {
		System.out.println("Je suis un animal");
	}
	
	public String cri() {
		return "...";
	}
	
	public final String origin() {
		return "La classe Animal est la m�re de toute les classes";
	}

}
