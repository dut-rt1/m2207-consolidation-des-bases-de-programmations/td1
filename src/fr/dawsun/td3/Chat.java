package fr.dawsun.td3;

public class Chat extends Animal{
	
	public void affiche() {
		System.out.println("Je suis un chat");
	}
	
	public String miauler() {
		return "Miaou !, Miaou !";
	}

}
