package fr.dawsun.td2;

public class TestRectangle {
	
	public static void main(String args[]) {
		
		Rectangle r = new Rectangle(2, 1, 5, 3);
		
		System.out.println("Surface du rectangle: " + r.getSurface());
		
		Rectangle r2 = new Rectangle(new Point(2,1), new Point(5,2));
		
		System.out.println("Surface deuxi�me rectangle: " + r2.getSurface());
		
	}
	

}
