package fr.dawsun.td2;

public class TestPoint {
	
	public static void main(String[] args) {
		
		Point p = new Point(2, 7);
		Point p2 = new Point();
		Point p3 = new Point(3, 4);
		Point p4 = new Point(6, 0);
		
		
		System.out.println("Coordonn�es du point (accesseurs) - x:" + p.getX() + ", y: " + p.getY());
		
		//change private to public
		//System.out.println("Coordonn�es du point (direct) - x:" + p.x + ", y: " + p.y);
		
		
		p2.seDecrire();
		
		//reset
		p2.resetCoordonnees();
		
		//show after reset
		System.out.println("Apr�s reset:");
		p2.seDecrire();
		
		//move the point
		p2.deplacer(5, 3);
		
		//show after moving
		System.out.println("Apr�s avoir bouger:");
		p2.seDecrire();
		
		//show distance between p3 and p4
		System.out.println("Distance entre p3 et p4: " + p3.distance(p4));
		
		//show pointCount
		System.out.println("Nombre de points cr�e: " + Point.getCpt());
		
		Point p5, p6, p7;
		
		p5 = new Point(2, 3);
		p6 = new Point(2, 3);
		p7 = p5;
		
		System.out.println("Test de l'�galit� des entit�es points:");
		
		if(p5 == p6) {
			System.out.println("p5 = p6");
		}else {
			System.out.println("p5 != p6");
		}
		
		if(p6 == p7) {
			System.out.println("p6 = p7");
		}else {
			System.out.println("p6 != p7");
		}
		
		System.out.println("Test de l'�galit� des valeurs des points:");
		
		if(p5.equals(p6)) {
			System.out.println("p5 = p6");
		}else {
			System.out.println("p5 != p6");
		}
		
		if(p6.equals(p7)) {
			System.out.println("p6 = p7");
		}else {
			System.out.println("p6 != p7");
		}
		
		
		
		
	}

}
