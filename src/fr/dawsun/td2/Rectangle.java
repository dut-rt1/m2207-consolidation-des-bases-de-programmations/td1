package fr.dawsun.td2;

public class Rectangle {
	
	private Point p1, p2;
	
	public Rectangle(int x1, int y1, int x2, int y2) {
		this.p1 = new Point(x1, y1);
		this.p2 = new Point(x2, y2);
	}
	
	public Rectangle(Point p1, Point p2) {
		this.p1 = p1;
		this.p2 = p2;
	}
	
	public double getSurface() {
		return (p2.getX()-p1.getX())*(p2.getY()-p1.getY());
	}

}
