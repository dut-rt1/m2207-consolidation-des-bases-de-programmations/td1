package fr.dawsun.td2;

public class Point {
	
	private int x, y;
	public static int compteur = 0;
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
		compteur++;
	}
	
	public Point() {
		this.x = 0;
		this.y = 0;
		compteur++;
	}
	
	public void deplacer(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void seDecrire() {
		System.out.println("Les coordonnées sont x = " + this.x + " et y = " + this.y);
	}

	public void resetCoordonnees() {
		this.x = 0;
		this.y = 0;
	}
	
	public boolean equals(Point p) {
		
		if(this.x == p.getX() && this.y == p.getY()) {
			return true;
		}
		
		return false;
		
	}
	
	public double distance(Point p) {
		return Math.sqrt((p.getX()-this.x)+(p.getY()-this.y));
	}
	
	public static int getCpt() {
		return compteur;
	}
	
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	

}
